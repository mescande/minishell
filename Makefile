# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mescande <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/02 14:03:19 by mescande          #+#    #+#              #
#    Updated: 2021/07/15 20:34:09 by matthieu         ###   ########.fr        #
#    Updated: 2021/07/15 18:08:06 by matthieu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	Minishell

MAKE		+= --no-print-directory

CC			?=	clang-9
CFLAGS		?=	-Wall -Wextra -Werror
#CFLAGS		+=	-g
#CFLAGS		+=	-fsanitize=address

LEN_NAME	=	`printf "%s" $(NAME) | wc -c`
DELTA		=	$$(echo "$$(tput cols)-32-$(LEN_NAME)" | bc)

SRC_DIR		=	srcs/
OBJ_DIR		=	objs/
INC_DIR		=	includes/
LIB_LIB		=	libft.a
LIB_DIR		=	libft/
LIB_INC		=	libft/includes/

INC_FLAGS	=	-iquote$(INC_DIR) -iquote$(LIB_INC) -L $(LIB_DIR) -l ft -lncursesw

SRC_LIST	=	minishell.c\
				init.c\
				new_shell_line.c\
				error_manager_0.c\
				error_manager_1.c\
				tree_create/tree.c\
				tree_create/add_1.c\
				tree_create/add_2.c\
				tree_create/add_3.c\
				tree_create/verification.c\
				dollar.c\
				expand_dollar.c\
				move_in_the_tree.c\
				handler.c\
				ft_separator.c\
				backslashes.c\
				history.c\
				termcaps/termcaps_handler.c\
				min_max.c\
				print_tree/print_tree.c\
				print_tree/tree_debug_buffer.c\
				print_tree/tree_debug_calcul.c\
				print_tree/tree_debug_core.c\
				termcaps/termcaps_arrows.c\
				termcaps/termcaps_others.c\
				termcaps/termcaps_move_a_lot.c\
				signal.c\
				builtins_files/cd.c\
				builtins_files/env.c\
				builtins_files/pwd.c\
				builtins_files/export.c\
				builtins_files/unset.c\
				builtins_files/exit.c\
				builtins_files/echo.c\
				verif_builtins.c\
				env_var.c\
				init_env_var.c\
				ft_pipe.c\
				fork.c\
				fd_dealer.c\
				exec_argv.c

SRC			=	$(addprefix $(SRC_DIR), $(SRC_LIST))
OBJ			=	$(addprefix $(OBJ_DIR), $(SRC_LIST:.c=.o))
DIR			=	$(sort $(dir $(OBJ)))
NB			=	$(words $(SRC_LIST))
INDEX		=	0

SHELL		:=	/bin/bash

all: 
	@$(MAKE) -j -C $(LIB_DIR) $(LIB_LIB)
	@$(MAKE) -j $(NAME)
	@printf "\r\033[38;5;117m✓ MAKE $@\033[0m\033[K\n"

$(NAME):		$(OBJ) Makefile $(LIB_DIR)$(LIB_LIB) 
	@$(CC) $(CFLAGS) -MMD $(OBJ) -o $@ $(INC_FLAGS)
	@printf "\r\033[38;5;117m✓ MAKE $(NAME)\033[0m\033[K\n"

$(OBJ_DIR)%.o:	$(SRC_DIR)%.c Makefile | $(DIR)
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo $$((20-$(INDEX)*20/$(NB) - 1))))
	@$(eval COLOR=$(shell list=(160 196 202 208 215 221 227 226 190 154 118 84 46); index=$$(($(PERCENT) * $${#list[@]} / 100)); echo "$${list[$$index]}"))
	@printf "\r\033[38;5;%dm↻ [%s]: %2d%% `printf '█%.0s' {0..$(DONE)}`%*s❙%s\033[0m\033[K" $(COLOR) $(NAME) $(PERCENT) $(TO_DO) "" "$(shell echo "$@" | sed 's/^.*\/\(.*\).[och]$$/\1/')"
	@$(CC) $(CFLAGS) -MMD -c $< -o $@ $(INC_FLAGS)
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))

$(DIR):
	@mkdir -p $@

clean:
	@$(MAKE) -C $(LIB_DIR) clean
	@rm -rf $(OBJ_DIR)
	@printf "\r\033[33;5;117m✓ MAKE $@\033[0m\033[K\n"

fclean: clean
	@$(MAKE) -C $(LIB_DIR) fclean
	@rm -rf $(NAME)
	@printf "\r\033[33;5;117m✓ MAKE $@\033[0m\033[K\n"

re: fclean
	@$(MAKE) all

nolib:
	@echo "Recompiling everything except libs"
	@rm -rf $(OBJ_DIR)
	@$(MAKE) -j $(NAME)

norme:
	norminette $(INC_DIR) $(SRC_DIR)
	@$(MAKE) -C $(LIB_DIR) norme

norminette: norme

test:
	@$(MAKE) all
	./$(NAME) $(ARGS)

valgrind:
	@$(MAKE) all
	@valgrind ./$(NAME) $(ARGS)

help:
	@echo "all	: compiling everything that changed, linking, not relinking\n"
	@echo "clean	: destroy all objects and linking files from program and libs\n"
	@echo "fclean	: clean and destroy exec files and libs"
	@echo "test	: all and exec with validfile.rt or a file given in argument"
	@echo "re	: fclean all"
	@echo "nolib	: destroy object of programs only (not lib) then compiling again"
	@echo "norme	: execute a norme test on all code files but do no compile"
	@echo "help	: print this help"
	@echo "test	: compile, and run the program with ARGS for argument (default : $$""(ARGS) = $(ARGS)"
	@echo "valgrind	: compile and run the program with valgrind and ARGS for argument"

.PHONY: all clean fclean re

-include $(OBJ:.o=.d)
