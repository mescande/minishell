/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/22 16:56:52 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 18:55:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	add_double_quote(t_tree **leaf, t_tree **root, char **str, int j)
{
	int	i;

	i = 1;
	(void)root;
	while (str[0][j + i] && !ft_is_new_sep(str[0], j + i, "\"\\"))
		i++;
	if (!str[0][j + i])
	{
		g_signal = -2;
		return (free_n_return("Non-ending double-quote", NULL, 1));
	}
	*str += j + ++i;
	return (cat_or_add(leaf, str[0] - i, i));
}

int	add_quote(t_tree **leaf, t_tree **root, char **str, int j)
{
	int	i;

	i = 1;
	(void)root;
	while (str[0][j + i] && str[0][j + i] != '\'')
		i++;
	if (!str[0][j + i])
	{
		g_signal = -2;
		return (free_n_return("Non-ending quote", NULL, 1));
	}
	*str += j + ++i;
	return (cat_or_add(leaf, str[0] - i, i));
}

int	eol(t_tree **leaf, t_tree **root, char **str, int j)
{
	leaf[0] = root[0];
	*str += j;
	if (!*str)
		return (free_n_return("Error : Multiple lines not managed",
				NULL, 1));
	return (0);
}

int	space(t_tree **leaf, t_tree **root, char **str, int j)
{
	(void)leaf;
	(void)root;
	*str += j + 1;
	cat_or_add(NULL, NULL, 3);
	return (0);
}

int	add_sep_2(t_tree **leaf, t_tree **root, char **str, int j)
{
	const char	*tmp = ft_strldup(*str + j, 2);

	if (!tmp)
		return (error_manager("strdup failed"));
	*str += j + 2;
	cat_or_add(NULL, NULL, 2);
	return (add_sep(leaf, root, (char *)tmp));
}
