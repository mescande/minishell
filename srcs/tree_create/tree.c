/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 10:25:55 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 20:26:33 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	add_root(t_tree **leaf, t_tree **root, char **str, int j)
{
	const char	*tmp = ft_strldup(*str + j, 1);
	t_tree		*new_tree;
	int			ret;

	if (!tmp)
		return (error_manager("strdup failed"));
	*str += j + 1;
	cat_or_add(NULL, NULL, 1);
	add_sep(leaf, root, (char *)tmp);
	ret = tree_it(&new_tree, *str);
	(*root)->right = new_tree;
	return (ret);
}

static const struct s_parse_assign_function	g_fct_list[] = {
	{"|", 1, add_sep_1},
	{";", 1, add_root},
	{">>", 2, add_sep_2},
	{"<", 1, add_sep_1},
	{">", 1, add_sep_1},
	{"'", 1, add_quote},
	{"\"", 1, add_double_quote},
	{"\n", 1, eol},
	{" ", 1, space},
	{"\0", 1, eol},
	{NULL, 0, 0}
};
/*
 * A ajouter peut-etre
 * {"<<", 2, add_sep_2},
*/

int	ft_is_new_sep(char *str, int j, char *seps)
{
	static int	status = 0;

	if (!str[j])
		return (1);
	if (ft_strchr(seps, str[j]))
	{
		if (str[j] == '\\' && status == 0)
		{
			status = 1;
			return (0);
		}
		else if (status == 1)
		{
			status = 0;
			return (0);
		}
		else
			return (1);
	}
	status = 0;
	return (0);
}

int	test_op(t_tree **root, int semi)
{
	t_tree	*tmp;

	if (!(*root))
		return (print_n_return("Syntax error 2", 1));
	if (!semi && (*root)->separator == 2 && !(*root)->right)
	{
		if (!test_op(&((*root)->left), 1))
		{
			tmp = *root;
			(*root) = (*root)->left;
			free(tmp->str);
			free(tmp);
		}
		else
			return (1);
	}
	else if ((*root)->separator)
	{
		if (!(*root)->right || !(*root)->left || !(*root)->left->str)
			return (print_n_return("Syntax error 3", 1));
		return (test_op(&((*root)->right), 2) + test_op(&((*root)->left), 2));
	}
	return (0);
}

static int	str_repartitor(char *str, t_tree **root, t_tree **leaf)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (*str && i != 1)
	{
		j = 0;
		while (!ft_is_new_sep(str, j, " ;|<>\"\'\n\\") && ft_isascii(str[j]))
			j++;
		if (!ft_isascii(str[j]))
			return (free_n_return("Non-ascii char", free_tree(root), 1));
		cat_or_add(leaf, str, j);
		i = 0;
		while (ft_strncmp(g_fct_list[i].id, str + j, g_fct_list[i].len))
			i++;
		if (!g_fct_list[i].id)
			return (free_n_return("Non-ascii char or unmanaged new line",
					free_tree(&root[0]), 1));
		else if (g_fct_list[i].fct(leaf, root, &str, j))
			return (free_n_return(NULL, free_tree(&root[0]), 1));
	}
	return (0);
}

int	tree_it(t_tree **root, char *line)
{
	int		res;
	t_tree	*leaf;

	if (!g_signal)
		return (g_signal);
	*root = ft_memalloc(sizeof(t_tree));
	if (!*root)
		return (error_manager("Root couldn't be create"));
	leaf = *root;
	res = str_repartitor(line, root, &leaf);
	reinitialised_redir_handler(leaf);
	cat_or_add(NULL, NULL, 1);
	if (g_signal == -1 && test_op(root, 0))
	{
		g_signal = -2;
		return (1);
	}
	return (res);
}
