/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verification.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 12:37:27 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/11 14:35:37 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	reinitialised_redir_handler(t_tree *leaf)
{
	ft_redir_handler(&leaf, NULL);
	ft_redir_handler(&leaf, NULL);
	return (0);
}
