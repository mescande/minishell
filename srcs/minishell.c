/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/20 14:48:35 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 16:34:08 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	g_signal = -1;

static void	main_loop(t_hist **list)
{
	struct termios	tattr;
	t_tree			*root;

	root = NULL;
	errno = 0;
	raw_terminal_mode(&tattr);
	new_shell_line(&root, list);
	default_terminal_mode(&tattr);
	if (g_signal == -1)
		move_in_the_tree(&root);
	free_tree(&root);
	if (g_signal < -1)
		g_signal = -1;
	printf("\n");
}

int	main(int argc, char **argv, char **envp)
{
	t_hist			*list;

	(void)argv;
	if (argc > 1)
		return (1);
	if (init_main(&list, envp))
		g_signal = 1;
	while (g_signal == -1)
		main_loop(&list);
	free_history(&list, 0);
	free_env();
	printf("exit\n\r");
	return (g_signal);
}
