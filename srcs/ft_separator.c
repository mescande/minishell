/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_separator.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 12:36:53 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 22:06:18 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	fd_input(t_fd *fd, t_tree *branch)
{
	int		ip_fd;

	if (!branch->right)
	{
		fd->status = 1;
		return (free_n_return("no such file or directory: (NULL)", NULL, 1));
	}
	else if (!(branch->right)->str)
	{
		fd->status = 1;
		return (free_n_return("no such file or directory: (NULL)", NULL, 1));
	}
	ip_fd = open((branch->right)->str, O_RDONLY);
	if (ip_fd == -1)
	{
		fd->status = 1;
		return (error_handler((branch->right)->str));
	}
	if (fd->input_fd)
		close(fd->input_fd);
	fd->input_fd = ip_fd;
	return (0);
}

static int	output_err(t_fd *fd, t_tree *branch)
{
	if (!branch->right)
	{
		fd->status = 1;
		return (free_n_return("syntax error near unexpected token 'newline'",
				NULL, 1));
	}
	else if (!(branch->right)->str)
	{
		fd->status = 1;
		return (free_n_return("syntax error near unexpected token 'newline'",
				NULL, 1));
	}
	return (0);
}

int	fd_output(t_fd *fd, t_tree *branch)
{
	int		op_fd;

	if (output_err(fd, branch))
		return (1);
	op_fd = open((branch->right)->str, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
	if (op_fd == -1)
	{
		fd->status = 1;
		return (error_handler((branch->right)->str));
	}
	if (fd->output_fd)
		close(fd->output_fd);
	fd->output_fd = op_fd;
	return (0);
}

int	fd_output_append(t_fd *fd, t_tree *branch)
{
	int		op_fd;

	if (output_err(fd, branch))
		return (1);
	op_fd = open((branch->right)->str, O_WRONLY | O_APPEND | O_CREAT, S_IRWXU);
	if (op_fd == -1)
	{
		fd->status = 1;
		return (error_handler((branch->right)->str));
	}
	if (fd->output_fd)
		close(fd->output_fd);
	fd->output_fd = op_fd;
	return (0);
}
