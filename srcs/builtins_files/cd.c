/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/03 20:33:43 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 18:15:06 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	dsp_if_needed(const int dsp, char *cwd)
{
	if (dsp && cwd)
		printf("%s\n", cwd);
	return (free_n_return(NULL, cwd, 0));
}

static int	execute_the_cd_to_this_awesome_directory(const int dsp,
		const char *dest, const char *curpwd)
{
	char	*cwd;
	char	*s_err;

	cwd = getcwd(NULL, 0);
	if (chdir(dest) == -1)
	{
		s_err = ft_strjoin("cd: ", dest);
		error_handler(s_err);
		free(s_err);
		return (free_n_return(NULL, cwd, 0));
	}
	if (cwd)
		ft_setenv("OLDPWD", cwd);
	else if (!cwd && curpwd)
		ft_setenv("OLDPWD", (char *)curpwd);
	else
		ft_unset(1, (char *[]){(char *)"OLDPWD"});
	free(cwd);
	cwd = getcwd(NULL, 0);
	if (cwd)
		ft_setenv("PWD", cwd);
	else
		ft_unset(1, (char *[]){(char *)"PWD"});
	return (dsp_if_needed(dsp, cwd));
}

int	ft_cd(int ac, char **av)
{
	const char	*home = ft_getenv("HOME");
	const char	*curpwd = ft_getenv("PWD");
	const char	*oldpwd = ft_getenv("OLDPWD");
	const char	*dest;
	int			dsp;

	dsp = 0;
	if (ac > 2)
		return (free_n_return("Too many arguments", NULL, 1));
	if (ac == 1)
	{
		if (!home)
			return (free_n_return("HOME not set", NULL, 1));
		dest = home;
	}
	else if (ac == 2 && !ft_memcmp(av[1], "-", 2))
	{
		if (!oldpwd)
			return (free_n_return("OLDPWD not set", NULL, 1));
		dest = oldpwd;
		dsp = 1;
	}
	else
		dest = av[1];
	return (execute_the_cd_to_this_awesome_directory(dsp, dest, curpwd));
}
