/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 17:03:54 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 14:28:51 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	react(char **av, int i, int *n)
{
	static int	showed = 0;
	int			j;

	if (!av)
	{
		showed = 0;
		return (0);
	}
	if (showed && write(1, " ", 1) == -1)
		return (error_handler(NULL));
	j = 1;
	while (!showed && av[i][0] == '-' && av[i][j] == 'n')
		j++;
	if (ft_strlen(av[i]) > 1 && av[i][j] == 0)
		(*n)++;
	else
	{
		if (write(1, av[i], ft_strlen(av[i])) == -1)
			return (error_handler(NULL));
		showed = 1;
	}
	return (0);
}

int	ft_echo(int ac, char **av)
{
	int	i;
	int	n;

	n = 0;
	i = 0;
	while (++i < ac)
	{
		if (react(av, i, &n))
			return (errno);
	}
	react(NULL, 0, &n);
	if (!n && write(1, "\n", 1) == -1)
		return (error_handler(NULL));
	return (0);
}
