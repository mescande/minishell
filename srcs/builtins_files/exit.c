/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/04 12:27:08 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/11 12:09:38 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_exit(int ac, char **av)
{
	int	val;

	if (ac == 1)
		val = 0;
	else if (ac == 2)
		val = ft_atoi(av[1]);
	else
		return (free_n_return("too many arguments", NULL, 1));
	g_signal = val;
	return (0);
}
