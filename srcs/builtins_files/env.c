/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/03 20:04:35 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/10 09:01:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_env(int ac, char **av)
{
	t_env	*p;

	(void) ac;
	(void) av;
	p = env_first(NULL);
	while (p)
	{
		if (printf("%s=%s\n", p->name, p->val) < 0)
		{
			dprintf(2, "%s\n", strerror(errno));
			return (errno);
		}
		p = p->next;
	}
	return (0);
}
