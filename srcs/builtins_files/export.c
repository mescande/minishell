/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/04 22:35:42 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 20:32:43 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_strjoin_nfree(char *s1, const char *s2)
{
	char	*res;

	if (!s1)
		return (ft_strdup(s2));
	if (!s2)
		return (s1);
	res = ft_strnew(ft_strlen(s2) + ft_strlen(s1));
	if (!res)
		return (res);
	res = ft_strcat(ft_strcpy(res, s1), s2);
	free(s1);
	return (res);
}

int	verif_name(char *av)
{
	int	j;

	j = 0;
	while (ft_is_envvar(av[j]) && av[j] != '=')
		j++;
	if (av[j] != '=' || j == 0)
		return (free_n_return("Not a valid identifier", NULL, 0));
	av[j++] = 0;
	return (j);
}

void	export_error(char *str)
{
	printf("export: `%s' is not a valid identifier\n", str);
}

int	ft_export(int ac, char **av)
{
	int		i;
	int		ret;
	char	*val;

	ret = 0;
	if (ac == 1)
		return (free_n_return("Not enough arguments", NULL, 1));
	i = 0;
	while (++i < ac)
	{
		val = ft_strchr(av[i], '=');
		if (!val)
		{
			export_error(av[i]);
			continue ;
		}
		ret = verif_name(av[i]);
		if (ret == 0 || av[i] + ret != val + 1)
			return (printf("%s\n", strerror(EINVAL)) * 0 + ret);
		*val = 0;
		ret = ft_setenv(av[i], val + 1);
		if (ret != 0)
			break ;
	}
	return (ret);
}
