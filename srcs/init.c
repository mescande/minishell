/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 17:39:41 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 11:33:57 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	raw_terminal_mode(struct termios *tattr_save)
{
	struct termios	tattr;

	signal(SIGINT, ctrl_c);
	signal(SIGQUIT, SIG_IGN);
	tcgetattr(STDIN_FILENO, &tattr);
	*tattr_save = tattr;
	tattr.c_lflag &= ~(ECHO | ICANON);
	tattr.c_oflag &= ~(OPOST);
	tattr.c_cc[VMIN] = 0;
	tattr.c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSANOW, &tattr);
}

void	default_terminal_mode(struct termios *tattr)
{
	signal(SIGINT, SIG_IGN);
	tcsetattr(STDIN_FILENO, TCSANOW, tattr);
}

static int	init_history(t_hist **list)
{
	char	*name;
	int		fd;
	int		gnl_val;

	name = history_filename();
	if (!name)
		return (error_handler("Couldn't find History"));
	fd = open(name, O_RDONLY);
	free(name);
	if (fd == -1)
		return (0);
	gnl_val = 1;
	while (gnl_val)
	{
		gnl_val = get_next_line(fd, &name);
		if (gnl_val == -1)
			return (free_n_return("GNL status -1", NULL, 1));
		if (gnl_val && add_elem(list, name))
			return (errno);
	}
	free(name);
	return (0);
}

int	init_main(t_hist **list, char **envp)
{
	char	*tmp;

	list[0] = NULL;
	init_env_var(envp);
	tmp = ft_getenv("TERM");
	if (!tmp)
		return (free_n_return("TERM must be setted\n", NULL, 1));
	if (tgetent(NULL, tmp) != 1)
		return (free_n_return("Termcaps library couldn't be loaded", NULL, 1));
	if (init_history(list))
		return (free_history(list, 1));
	return (0);
}

int	init_parse(t_tree *t)
{
	t = ft_memalloc(sizeof(t_tree));
	if (!t)
		return (error_manager("Parsing tree initialisation failed\n"));
	return (0);
}
