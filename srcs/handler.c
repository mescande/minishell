/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 12:36:53 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 16:08:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	semi_handler(t_tree *branch)
{
	if (branch && branch->separator && !ft_strcmp(branch->str, ";"))
	{
		semi_handler(branch->left);
		semi_handler(branch->right);
	}
	else if (branch)
		ft_semicolon(branch);
	return (0);
}

static t_fd	*skip_fd(t_fd *fd, int skip)
{
	int	i;

	i = 0;
	while (i < skip)
	{
		fd = fd->next;
		++i;
	}
	return (fd);
}

int	pipe_handler(t_fd *fd, t_tree *branch, int *skip, int piped)
{
	if (branch && branch->separator)
	{
		if (pipe_handler(fd, branch->left, skip, piped))
			return (1);
		if (!ft_strcmp(branch->str, "|"))
			return (pipe_handler(fd, branch->right, skip, 1));
	}
	else if (branch)
	{
		fd = skip_fd(fd, *skip);
		*skip = *skip + 1;
		if (!branch->separator && branch->str != NULL)
			return (ft_pipe(fd->next, branch, piped));
	}
	return (0);
}
