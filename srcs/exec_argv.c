/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_argv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 12:36:53 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 11:13:39 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*binpath(char *dir, char *filename)
{
	char	*tmp;
	char	*path;

	tmp = ft_strjoin(dir, "/");
	path = ft_strjoin(tmp, filename);
	free(tmp);
	tmp = NULL;
	return (path);
}

static char	*check_dir(char *filename, char **dir_table, DIR *dir, int i)
{
	char			*ret;
	struct dirent	*ent;
	struct stat		statbuf;

	ret = NULL;
	ent = readdir(dir);
	while (ent)
	{
		if (!ft_strcmp(ent->d_name, filename))
		{
			ret = binpath(*(dir_table + i), filename);
			stat(ret, &statbuf);
			if (statbuf.st_mode & S_IXOTH)
				return (ret);
			free(ret);
			ret = NULL;
		}
		ent = readdir(dir);
	}
	return (NULL);
}

char	*get_binpath(char *filename)
{
	int				i;
	char			*ret;
	DIR				*dir;
	char			**dir_table;

	i = 0;
	ret = NULL;
	if (filename_is_builtin(filename))
		return (ft_strdup(filename));
	dir_table = ft_strsplit(ft_getenv("PATH"), ':');
	if (dir_table == NULL)
		return (NULL);
	while (!ret && *(dir_table + i) != NULL)
	{
		dir = opendir(*(dir_table + i));
		if (dir)
			ret = check_dir(filename, dir_table, dir, i);
		closedir(dir);
		i++;
	}
	free_table(dir_table);
	dir_table = NULL;
	return (ret);
}

static char	**exec_argv_(t_tree *branch, char *binpath, char **argv)
{
	int			count;
	t_tree		*tmp;

	count = 0;
	tmp = branch;
	while (tmp != NULL)
	{
		if (count == 0)
			*(argv + count) = binpath;
		else
		{
			*(argv + count) = tmp->str;
			tmp = tmp->left;
		}
		count++;
	}
	*(argv + count) = NULL;
	return (argv);
}

char	**exec_argv(t_tree *branch)
{
	char		*binpath;
	char		**argv;
	t_tree		*tmp;
	int			count;

	count = 0;
	tmp = branch;
	if (!ft_strchr(branch->str, '/'))
		binpath = get_binpath(branch->str);
	else if (branch->str != NULL)
		binpath = ft_strdup(branch->str);
	else
		return (NULL);
	while (tmp != NULL)
	{
		count++;
		if (tmp->str == NULL)
			break ;
		tmp = tmp->left;
	}
	argv = ft_memalloc(sizeof(char *) * (count + 2));
	if (argv == NULL)
		return (NULL);
	return (exec_argv_(branch, binpath, argv));
}
