/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_manager_0.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 13:24:05 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/12 13:21:14 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	*free_tree(t_tree **root)
{
	if (!*root)
		return (NULL);
	if ((*root)->left)
		free_tree(&(*root)->left);
	if ((*root)->right)
		free_tree(&(*root)->right);
	free((*root)->str);
	(*root)->str = NULL;
	(*root)->left = NULL;
	(*root)->right = NULL;
	free(*root);
	*root = NULL;
	return (NULL);
}

int	free_history(t_hist **elem, int val)
{
	t_hist	*save;
	t_hist	*tmp;

	tmp = elem[0];
	while (tmp && tmp->next)
		tmp = tmp->next;
	while (tmp)
	{
		save = tmp->prev;
		free(tmp->line);
		free(tmp->edit.str);
		free(tmp);
		tmp = save;
	}
	elem[0] = NULL;
	return (val);
}

void	free_env(void)
{
	t_env	*p;
	t_env	*tmp;

	p = env_first(NULL);
	while (p)
	{
		free(p->name);
		free(p->val);
		tmp = p->next;
		ft_bzero(p, sizeof(t_env));
		free(p);
		p = tmp;
	}
	return ;
}

int	free_n_return(char *str, void *to_free, int val)
{
	if (str)
		dprintf(2, "\033[33m%s\n\r\033[0m", str);
	free(to_free);
	to_free = NULL;
	return (val);
}

int	error_handler(char *str)
{
	if (str && !errno)
		dprintf(2, "\033[33m%s\n\r\033[0m", str);
	else if (str && errno)
		dprintf(2, "\033[33m%s: \033[31m%s\n\r\033[0m", str, strerror(errno));
	else if (!str && errno)
		dprintf(2, "\033[31m%s\n\r\033[0m", strerror(errno));
	return (errno);
}
