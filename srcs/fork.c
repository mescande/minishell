/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 12:36:53 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 11:05:09 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	**envp_table(char **envp, int count)
{
	int		i;
	char	*env_tmp;
	char	*env_line;
	t_env	*env;

	i = 0;
	env = env_first(NULL);
	while (count)
	{
		env_tmp = ft_strjoin(env->name, "=");
		env_line = ft_strjoin(env_tmp, env->val);
		free(env_tmp);
		env_tmp = NULL;
		*(envp + i) = env_line;
		i++;
		count--;
		env = env->next;
	}
	*(envp + i) = NULL;
	return (envp);
}

static char	**envp_setup(void)
{
	char	**envp;
	int		count;
	t_env	*env;

	count = 0;
	env = env_first(NULL);
	while (env)
	{
		count++;
		env = env->next;
	}
	envp = malloc(sizeof(char *) * (count + 1));
	if (envp == NULL)
		return (NULL);
	envp_table(envp, count);
	return (envp);
}

static void	execution(t_fd *fd, char **argv)
{
	int			builtin_exec;
	char		**envp;

	builtin_exec = verif_builtins(fd, argv, 1);
	if (!builtin_exec && *argv)
	{
		envp = envp_setup();
		execve(*argv, argv + 1, envp);
		ft_putstr_fd("Minishell: ", 2);
		ft_putstr_fd(argv[1], 2);
		ft_putstr_fd(": \033[31m", 2);
		ft_putstr_fd(strerror(errno), 2);
		ft_putstr_fd("\033[0m\n", 2);
		free_table(envp);
		fd->status = errno;
	}
	else if (!builtin_exec)
	{
		ft_putstr_fd("Minishell: ", 2);
		ft_putstr_fd(argv[1], 2);
		ft_putstr_fd(": \033[31mcommand not found\033[0m\n", 2);
		fd->status = 127;
	}
}

void	child_process(t_fd *fd, char **argv)
{
	int	status;

	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	close(fd->fd_[0]);
	if (fd->output_fd)
		dup2(fd->output_fd, STDOUT_FILENO);
	else if (fd->next)
		dup2(fd->fd_[1], STDOUT_FILENO);
	execution(fd, argv);
	status = fd->status;
	free_fd(fd);
	free(*argv);
	*argv = NULL;
	free(argv);
	argv = NULL;
	exit(status);
}

void	parent_process(int cpid, t_fd *fd)
{
	close(fd->fd_[1]);
	dup2(fd->fd_[0], STDIN_FILENO);
	fd->cpid = cpid;
}
