/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 10:20:58 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/26 10:24:14 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	print_tree(t_tree *tree)
{
	return (_print_tree(tree, 0, 1));
}

int	pretty_print_tree(t_tree *tree)
{
	return (_print_tree(tree, 1, 1));
}

int	dprint_tree(int fd, t_tree *tree)
{
	return (_print_tree(tree, 0, fd));
}

int	dpretty_print_tree(int fd, t_tree *tree)
{
	return (_print_tree(tree, 1, fd));
}
