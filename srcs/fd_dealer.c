/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fd_dealer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <shlu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/05 10:25:10 by user42            #+#    #+#             */
/*   Updated: 2021/07/15 20:26:55 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	create_fd(t_fd *fd, t_tree *branch)
{
	t_fd	*new;

	(void) branch;
	new = ft_memalloc(sizeof(t_fd));
	if (!new)
		return (1);
	if (pipe(new->fd_) == -1)
		return (1);
	new->next = fd->next;
	fd->next = new;
	return (0);
}

static int	fd_redirector(t_fd *fd_now, t_tree *branch)
{
	spend_money(&branch->right);
	if (branch->right->left || branch->right->right || !*branch->right->str)
		return (print_n_return("Minishell: ambiguous redirect", 1));
	if (!ft_strcmp(branch->str, "<"))
		return (fd_input(fd_now, branch));
	else if (!ft_strcmp(branch->str, ">"))
		return (fd_output(fd_now, branch));
	else if (!ft_strcmp(branch->str, ">>"))
		return (fd_output_append(fd_now, branch));
	return (0);
}

int	fd_dealer(t_fd *fd, t_tree *branch, int already_create)
{
	if (!branch)
		return (0);
	if (!branch->separator && !already_create)
		return (create_fd(fd, branch));
	if (!branch->separator)
		return (0);
	if (!already_create && create_fd(fd, branch))
		return (1);
	if (!ft_strcmp(branch->str, "|"))
		return (fd_dealer(fd, branch->left, 0));
	else
	{
		if (branch->left->separator)
			if (fd_dealer(fd, branch->left, 1))
				return (1);
		if (fd_redirector(fd->next, branch))
			return (1);
	}
	return (0);
}
