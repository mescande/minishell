/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcaps_move_a_lot.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 22:54:12 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/07 18:27:32 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	home(t_hist **wo, int *buf_pos)
{
	const t_hist	*w = *wo;

	while (w->edit.mov > w->edit.str)
		move_left(wo, buf_pos);
	return (0);
}

int	end(t_hist **wo, int *buf_pos)
{
	const t_hist	*w = *wo;

	while (w->edit.mov < w->edit.s)
		move_right(wo, buf_pos);
	return (0);
}

int	ctrl_left(t_hist **wo, int *buf_pos)
{
	const t_hist	*w = *wo;

	(void)buf_pos;
	if (w->edit.mov > w->edit.str
		&& !ft_isspace(*w->edit.mov) && ft_isspace(*(w->edit.mov - 1)))
		move_left(wo, buf_pos);
	while (w->edit.mov > w->edit.str && ft_isspace(*w->edit.mov))
		move_left(wo, buf_pos);
	while (w->edit.mov > w->edit.str && !ft_isspace(*(w->edit.mov - 1)))
		move_left(wo, buf_pos);
	return (0);
}

int	ctrl_right(t_hist **wo, int *buf_pos)
{
	const t_hist	*w = *wo;

	(void)buf_pos;
	while (w->edit.mov < w->edit.s && ft_isspace(*w->edit.mov))
		move_right(wo, buf_pos);
	while (w->edit.mov < w->edit.s && !ft_isspace(*(w->edit.mov)))
		move_right(wo, buf_pos);
	return (0);
}
