/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcaps_handler.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/22 13:29:33 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/15 15:41:45 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static const struct s_termcaps	g_keys[] = {
	{"  up_arrow", "\x1B\x5B\x41", 3, history_up},
	{"down_arrow", "\x1B\x5B\x42", 3, history_down},
	{"left_arrow", "\x1B\x5B\x44", 3, move_left},
	{"righ_arrow", "\x1B\x5B\x43", 3, move_right},
	{"backspace ", "\x7f", 1, backspace},
	{"del       ", "\x1b\x5b\x33\x7e", 4, del},
	{"ctrl_left ", "\x1b\x5b\x31\x3b\x35\x44", 6, ctrl_left},
	{"ctrl_right", "\x1b\x5b\x31\x3b\x35\x43", 6, ctrl_right},
	{"home      ", "\x1b\x5b\x48", 4, home},
	{"end       ", "\x1b\x5b\x46", 4, end},
	{"NULL      ", NULL, 0, 0}
};

int	putint(int c)
{
	return (write(1, &c, 1));
}

static int	termcaps_verif(char *buff, t_hist **wo, int *i, int *buf_pos)
{
	int	j;

	j = -1;
	while (g_keys[++j].len)
	{
		if (!strncmp(g_keys[j].value, buff + *i, g_keys[j].len))
		{
			if (g_keys[j].fct(wo, buf_pos))
				return (errno);
			*i += (g_keys[j].len - 1);
			break ;
		}
	}
	return (0);
}

static int	ctrl_d_manager(t_hist **wo, char *buff)
{
	(void)buff;
	if ((*wo)->edit.s == (*wo)->edit.str)
	{
		g_signal = 0;
		return (1);
	}
	return (0);
}

int	not_a_great_buffer(char *buff, t_hist *wo, int *buf_pos)
{
	while (*buff)
		++buff;
	if (update_line(wo, buf_pos))
		return (errno);
	return (0);
}

int	termcaps_handler(char *buff, t_hist **wo)
{
	int		buf_pos;
	int		i;

	i = -1;
	buf_pos = (*wo)->edit.s - (*wo)->edit.str;
	while (buff[++i])
	{
		if (ft_isprint(buff[i]) || buff[i] == '\n')
		{
			if (buffer_append_raw(&(*wo)->edit, buff + i, 1))
				return (error_manager("Strnjoin failed; abort"));
		}
		else if (buff[i] == 4 && ctrl_d_manager(wo, buff))
			return (0);
		else if (termcaps_verif(buff, wo, &i, &buf_pos))
			return (errno);
		else if (buff[i] == '\x1B' && buff[i + 1])
			return (not_a_great_buffer(buff + i, *wo, &buf_pos));
	}
	return (0);
}
