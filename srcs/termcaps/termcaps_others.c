/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcaps_others.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 19:53:55 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/07 18:27:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	remove_char(t_hist **wo)
{
	ft_memmove((*wo)->edit.mov, (*wo)->edit.mov + 1,
		(*wo)->edit.s - (*wo)->edit.mov);
	if ((*wo)->edit.mov == (*wo)->edit.s)
		(*wo)->edit.mov--;
	(*wo)->edit.s--;
	*((*wo)->edit.s) = 0;
	if (tputs(tgetstr("dc", NULL), 1, putint) == ERR)
		return (error_manager("tputs failed"));
	return (0);
}

int	del(t_hist **wo, int *buf_pos)
{
	(void)buf_pos;
	if ((*wo)->edit.mov < (*wo)->edit.s && (*wo)->edit.mov >= (*wo)->edit.str)
		return (remove_char(wo));
	return (0);
}

int	backspace(t_hist **wo, int *buf_pos)
{
	if ((*wo)->edit.mov > (*wo)->edit.str)
	{
		move_left(wo, buf_pos);
		return (remove_char(wo));
	}
	return (0);
}
