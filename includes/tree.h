/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 09:25:15 by matthieu          #+#    #+#             */
/*   Updated: 2021/07/08 22:14:36 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TREE_H
# define TREE_H

# include "minishell.h"

# define MAX_WORD_WIDTH 10
# define WORD_ADDED_WIDTH 3

typedef struct s_binary_tree_elem	t_tree;

typedef struct s_tree_display {
	int		block_width;
	int		block_delta;
	int		labl_width;
	int		label_delta;
	int		lwidth;
	int		lpos;
	int		rwidth;
	int		rpos;
	int		trunc;
	int		depth;
	int		pos;
}	t_tree_display;

int		print_tree(t_tree *tree);
int		pretty_print_tree(t_tree *tree);
int		dprint_tree(int fd, t_tree *tree);
int		dpretty_print_tree(int fd, t_tree *tree);

#endif
